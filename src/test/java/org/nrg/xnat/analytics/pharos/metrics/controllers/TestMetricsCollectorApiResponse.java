package org.nrg.xnat.analytics.pharos.metrics.controllers;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.nrg.xnat.analytics.pharos.metrics.services.MetricsReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.time.ZoneOffset.UTC;
import static lombok.AccessLevel.PRIVATE;
import static org.mockito.Mockito.when;

@SpringBootTest
@WebMvcTest(MetricsCollectorApi.class)
@Getter(PRIVATE)
@Accessors(prefix = "_")
public class TestMetricsCollectorApiResponse {
    private static final long                 START        = OffsetDateTime.of(2020, 1, 1, 0, 0, 0, 0, UTC).toEpochSecond();
    private static final long                 END          = OffsetDateTime.of(2022, 1, 1, 0, 0, 0, 0, UTC).toEpochSecond();
    private static final List<MetricsReport>  MOCK_REPORTS = IntStream.range(1, 6)
                                                                      .mapToObj(index -> MetricsReport.builder()
                                                                                                      .uuid(UUID.randomUUID())
                                                                                                      .ipAddress("10.1.1." + index)
                                                                                                      .xnatVersion("1.7.5." + index)
                                                                                                      .build())
                                                                      .collect(Collectors.toList());

    private final        MockMvc              _mvc;

    @Mock
    private              MetricsReportService _service;

    @Autowired
    TestMetricsCollectorApiResponse(final MockMvc mvc) {
        _mvc = mvc;
    }

    @SuppressWarnings("unused")
    private static OffsetDateTime randomDateTime() {
        return OffsetDateTime.of(LocalDateTime.ofEpochSecond(ThreadLocalRandom.current().nextLong(START, END), 0, UTC), UTC);
    }

    @BeforeEach
    public void setUp() {
        when(_service.getAll()).thenReturn(MOCK_REPORTS);
        MOCK_REPORTS.forEach(report -> when(_service.get(report.getId())).thenReturn(report));
    }
}
