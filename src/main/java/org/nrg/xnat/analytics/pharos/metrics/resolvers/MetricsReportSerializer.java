package org.nrg.xnat.analytics.pharos.metrics.resolvers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class MetricsReportSerializer extends StdSerializer<MetricsReport> {
    @Autowired
    public MetricsReportSerializer() {
        super(MetricsReport.class);
        log.info("Created Metrics report serializer");
    }

    @Override
    public void serialize(final MetricsReport value, final JsonGenerator generator, final SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeObjectField("uuid", value.getUuid());
        generator.writeStringField("xnatVersion", value.getXnatVersion());
        generator.writeStringField("ipAddress", value.getIpAddress());
        if (value.getId() != null) {
            generator.writeNumberField("id", value.getId());
        }
        if (value.getCreated() != null) {
            generator.writeObjectField("created", value.getCreated());
        }
        if (value.getCreatedBy() != null) {
            generator.writeObjectField("createdBy", value.getCreatedBy());
        }
        final JsonNode reports = value.getProviderReports();
        if (!reports.isEmpty()) {
            generator.writeArrayFieldStart("providerReports");
            reports.forEach(node -> {
                try {
                    generator.writeObject(node);
                } catch (IOException e) {
                    log.error("Failed to write node {}", node, e);
                }
            });
            generator.writeEndArray();
        }
        generator.writeEndObject();
    }
}
