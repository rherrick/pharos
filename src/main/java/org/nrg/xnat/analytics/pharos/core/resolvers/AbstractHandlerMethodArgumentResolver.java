package org.nrg.xnat.analytics.pharos.core.resolvers;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import org.nrg.xnat.analytics.pharos.core.domain.ImmutableEntity;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.*;
import java.util.stream.IntStream;
import javax.servlet.http.HttpServletRequest;

import static lombok.AccessLevel.PROTECTED;

@Getter(PROTECTED)
@Slf4j
public abstract class AbstractHandlerMethodArgumentResolver<A extends Annotation, E extends ImmutableEntity> implements HandlerMethodArgumentResolver {
    private final Class<? extends Annotation> _annotation;

    // TODO: Eventually use Reflection.getParameterizedTypeForClass() to get this without requiring specifying it in both class declaration and constructor.
    protected AbstractHandlerMethodArgumentResolver(final Class<A> annotation) {
        _annotation = annotation;
    }

    protected static byte[] convertMultipartFilesToArray(final List<MultipartFile> files) {
        final Byte[] bytes = files.stream()
                                  .map(AbstractHandlerMethodArgumentResolver::convertMultipartFileToList)
                                  .flatMap(List::stream)
                                  .toArray(Byte[]::new);
        final byte[] converted = new byte[bytes.length];
        IntStream.range(0, bytes.length).forEach(index -> converted[index] = bytes[index]);
        return converted;
    }

    private static List<Byte> convertMultipartFileToList(final MultipartFile multipartFile) {
        try {
            return Arrays.asList(ArrayUtils.toObject(multipartFile.getBytes()));
        } catch (IOException e) {
            log.warn("An error occurred trying to convert multipart file {} to byte array", multipartFile.getName(), e);
            return Collections.emptyList();
        }
    }

    protected abstract E handleArgument(final HttpServletRequest request, final Map<String, String[]> parameters, final Map<String, List<MultipartFile>> files);

    @Override
    public boolean supportsParameter(final @NotNull MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(_annotation) != null;
    }

    @Override
    public E resolveArgument(final @NotNull MethodParameter methodParameter,
                             final ModelAndViewContainer modelAndViewContainer,
                             final @NotNull NativeWebRequest nativeWebRequest,
                             final WebDataBinderFactory webDataBinderFactory) {
        final HttpServletRequest    request    = (HttpServletRequest) nativeWebRequest.getNativeRequest();
        final Map<String, String[]> parameters = request.getParameterMap();
        final Map<String, List<MultipartFile>> files = request instanceof MultipartRequest
                                                       ? ((MultipartRequest) request).getMultiFileMap()
                                                       : Collections.emptyMap();

        if (log.isInfoEnabled()) {
            log.info("Request has {} parameters:", parameters.size());
            final Set<String> parameterNames = parameters.keySet();
            parameterNames.forEach(key -> log.info(" * {}: {}", key, parameters.get(key)));
            log.info("Request has {} files:", files.size());
            final Set<String> fileNames = files.keySet();
            fileNames.forEach(key -> log.info(" * {}: {} files", key, files.get(key).size()));
        }

        return handleArgument(request, parameters, files);
    }

    protected String getJoinedParameters(final Map<String, String[]> parameters, final String keyGuid) {
        final String[] elements = parameters.get(keyGuid);
        return elements != null ? String.join(", ", elements) : "";
    }
}
