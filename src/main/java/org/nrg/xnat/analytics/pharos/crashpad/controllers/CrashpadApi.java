package org.nrg.xnat.analytics.pharos.crashpad.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.core.NotFoundException;
import org.nrg.xnat.analytics.pharos.core.controllers.AbstractPharosApi;
import org.nrg.xnat.analytics.pharos.crashpad.annotations.CrashReportParam;
import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.domain.projections.SimplifiedCrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.services.CrashReportService;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;
import static org.springframework.http.MediaType.*;

@Tag(name = "Crashpad", description = "Provides the API for submitting Crashpad reports")
@RestController
@RequestMapping("/crashpad")
@Getter(PROTECTED)
@Setter(PRIVATE)
@Accessors(prefix = "_")
@Slf4j
public class CrashpadApi extends AbstractPharosApi {
    private final CrashReportService _service;

    public CrashpadApi(final CrashReportService service) {
        _service = service;
        log.info("Created Crashpad API");
    }

    @Operation(summary = "Gets a list of all submitted Crashpad reports", description = "This can return a lot of data and should be used carefully.")
    @ApiResponses(@ApiResponse(responseCode = "200", description = "The Crashpad reports were successfully returned",
                               content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = SimplifiedCrashReport.class)),
                                          @Content(mediaType = APPLICATION_XML_VALUE, schema = @Schema(implementation = SimplifiedCrashReport.class))}))
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public Page<SimplifiedCrashReport> getCrashReports(final @RequestParam(required = false, defaultValue = "0") int page,
                                                       final @RequestParam(required = false, defaultValue = "0") int size) {
        return getService().getAllSimplifiedReports(page(page, size));
    }

    @Operation(summary = "Gets the Crashpad report with the specified ID",
               parameters = @Parameter(name = "id", in = ParameterIn.PATH, required = true))
    @ApiResponses({@ApiResponse(responseCode = "200", description = "The Crashpad report was successfully returned",
                                content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = SimplifiedCrashReport.class)),
                                           @Content(mediaType = APPLICATION_XML_VALUE, schema = @Schema(implementation = SimplifiedCrashReport.class))}),
                   @ApiResponse(responseCode = "403", description = "The user requested a report that is not accessible to them"),
                   @ApiResponse(responseCode = "404", description = "The user requested a report that does not exist")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(path = "id/{id}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public SimplifiedCrashReport getCrashReport(final @PathVariable long id) {
        return Optional.ofNullable(getService().get(id)).orElseThrow(() -> new NotFoundException(SimplifiedCrashReport.class, id));
    }

    @Operation(summary = "Gets all Crashpad reports associated with the specified GUID",
               description = "GUIDs are unique to a particular installed application, so this essentially returns all reports from a particular instance",
               parameters = @Parameter(name = "guid", in = ParameterIn.PATH, required = true, schema = @Schema(implementation = UUID.class)))
    @ApiResponses({@ApiResponse(responseCode = "200", description = "The Crashpad reports for the specified GUID were successfully returned"),
                   @ApiResponse(responseCode = "403", description = "The user requested reports for a GUID that is not accessible to them"),
                   @ApiResponse(responseCode = "404", description = "The user requested reports for a GUID that does not exist")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(path = "guid/{guid}", produces = APPLICATION_JSON_VALUE)
    public Page<SimplifiedCrashReport> getCrashReportsByGuid(final @PathVariable String guid,
                                                             final @RequestParam(required = false, defaultValue = "0") int page,
                                                             final @RequestParam(required = false, defaultValue = "0") int size) {
        return Optional.ofNullable(getService().findSimplifiedReportsByGuid(guid, page(page, size)))
                       .orElseThrow(() -> new NotFoundException(SimplifiedCrashReport.class, guid));
    }

    @Operation(summary = "Gets the full Crashpad report with the specified ID",
               parameters = @Parameter(name = "id", in = ParameterIn.PATH, required = true))
    @ApiResponses({@ApiResponse(responseCode = "200", description = "The Crashpad report was successfully returned",
                                content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = CrashReport.class)),
                                           @Content(mediaType = APPLICATION_XML_VALUE, schema = @Schema(implementation = CrashReport.class))}),
                   @ApiResponse(responseCode = "403", description = "The user requested a report that is not accessible to them"),
                   @ApiResponse(responseCode = "404", description = "The user requested a report that does not exist")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(path = "id/{id}/full", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public CrashReport getFullCrashReport(final @PathVariable long id) {
        return Optional.ofNullable(getService().getFullCrashReport(id)).orElseThrow(() -> new NotFoundException(CrashReport.class, id));
    }

    @Operation(summary = "Gets all full Crashpad reports associated with the specified GUID",
               description = "GUIDs are unique to a particular installed application, so this essentially returns all reports from a particular instance",
               parameters = @Parameter(name = "guid", in = ParameterIn.PATH, required = true, schema = @Schema(implementation = UUID.class)))
    @ApiResponses({@ApiResponse(responseCode = "200", description = "The Crashpad reports for the specified GUID were successfully returned"),
                   @ApiResponse(responseCode = "403", description = "The user requested reports for a GUID that is not accessible to them"),
                   @ApiResponse(responseCode = "404", description = "The user requested reports for a GUID that does not exist")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(path = "guid/{guid}/full", produces = APPLICATION_JSON_VALUE)
    public Page<CrashReport> getFullCrashReportsByGuid(final @PathVariable String guid,
                                                       final @RequestParam(required = false, defaultValue = "0") int page,
                                                       final @RequestParam(required = false, defaultValue = "0") int size) {
        return Optional.ofNullable(getService().findReportsByGuid(guid, page(page, size))).orElseThrow(() -> new NotFoundException(CrashReport.class, guid));
    }

    @Operation(summary = "Creates a new Crashpad report based on the submitted data",
               description = "This endpoint accepts a standard Crashpad report submission and stores it",
               parameters = @Parameter(name = "report", in = ParameterIn.DEFAULT, required = true, schema = @Schema(implementation = CrashReport.class)))
    @ApiResponses({@ApiResponse(responseCode = "200", description = "The submitted Crashpad report was successfully saved"),
                   @ApiResponse(responseCode = "500", description = "An error occurred trying to save the report")})
    @PostMapping(consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public SimplifiedCrashReport handleCrashReport(final @CrashReportParam CrashReport report) {
        getService().create(report);
        return getService().get(report.getId());
    }
}
