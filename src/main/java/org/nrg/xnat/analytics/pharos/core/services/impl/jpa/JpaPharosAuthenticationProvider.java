package org.nrg.xnat.analytics.pharos.core.services.impl.jpa;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.core.services.PharosAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
public class JpaPharosAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider implements PharosAuthenticationProvider {
    private final JpaPharosUserDetailsManager _userDetailsManager;
    private final PasswordEncoder             _encoder;
    private final String                      _userNotFoundEncodedPassword;

    @Autowired
    public JpaPharosAuthenticationProvider(final JpaPharosUserDetailsManager userDetailsManager, final PasswordEncoder encoder) {
        _userDetailsManager          = userDetailsManager;
        _encoder                     = encoder;
        _userNotFoundEncodedPassword = _encoder.encode("userNotFoundPassword");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void additionalAuthenticationChecks(final UserDetails user, final UsernamePasswordAuthenticationToken token) throws AuthenticationException {
        if (token.getCredentials() == null) {
            log.debug("Failed to authenticate since no credentials provided");
            throw new BadCredentialsException("Bad credentials");
        }
        if (!_encoder.matches(token.getCredentials().toString(), user.getPassword())) {
            log.debug("Failed to authenticate since password does not match stored value");
            throw new BadCredentialsException("Bad credentials");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected UserDetails retrieveUser(final String username, final UsernamePasswordAuthenticationToken token) throws AuthenticationException {
        try {
            return _userDetailsManager.loadUserByUsername(username);
        } catch (UsernameNotFoundException e) {
            mitigateAgainstTimingAttack(token);
            throw e;
        }
    }

    private void mitigateAgainstTimingAttack(final UsernamePasswordAuthenticationToken token) {
        if (token.getCredentials() != null) {
            final String presentedPassword = token.getCredentials().toString();
            _encoder.matches(presentedPassword, _userNotFoundEncodedPassword);
        }
    }
}
