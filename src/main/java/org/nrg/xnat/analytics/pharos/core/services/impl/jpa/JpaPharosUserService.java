package org.nrg.xnat.analytics.pharos.core.services.impl.jpa;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.core.domain.Group;
import org.nrg.xnat.analytics.pharos.core.domain.Submitter;
import org.nrg.xnat.analytics.pharos.core.domain.User;
import org.nrg.xnat.analytics.pharos.core.repositories.GroupRepository;
import org.nrg.xnat.analytics.pharos.core.repositories.SubmitterRepository;
import org.nrg.xnat.analytics.pharos.core.repositories.UserRepository;
import org.nrg.xnat.analytics.pharos.core.services.PharosUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
public class JpaPharosUserService implements PharosUserService {
    private final UserRepository      _users;
    private final GroupRepository     _groups;
    private final SubmitterRepository _submitters;

    @Autowired
    public JpaPharosUserService(final UserRepository users, final GroupRepository groups, final SubmitterRepository submitters) {
        _users      = users;
        _groups     = groups;
        _submitters = submitters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User save(final User user) {
        return _users.saveAndFlush(user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Group save(final Group group) {
        return _groups.saveAndFlush(group);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Submitter save(final Submitter submitter) {
        return _submitters.saveAndFlush(submitter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> findAllUsers() {
        return _users.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Group> findAllGroups() {
        return _groups.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Submitter> findAllSubmitters() {
        return _submitters.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Submitter> findActiveSubmitters() {
        return _submitters.findActiveSubmitters();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> findUserByName(final String username) {
        return Optional.ofNullable(_users.findByUsername(username));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Group> findGroupByName(final String groupName) {
        return Optional.ofNullable(_groups.findByGroupName(groupName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Submitter> findSubmitterByUuid(final String uuid) {
        return Optional.ofNullable(_submitters.findByUuid(uuid));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsUserByName(final String name) {
        return _users.existsByUsername(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsGroupByName(final String name) {
        return _groups.existsByGroupName(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsSubmitterByUuid(String uuid) {
        return _submitters.existsByUuid(uuid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteUserByName(String username) {
        _users.deleteByUsername(username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteGroupByName(final String groupName) {
        _groups.deleteByGroupName(groupName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSubmitterByUuid(final String uuid) {
        _submitters.deleteByUuid(uuid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addUserToGroup(final String username, final String groupName) {
        findGroupByName(groupName).ifPresent(group -> findUserByName(username).ifPresent(user -> {
            group.getUsers().add(user);
            save(group);
        }));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeUserFromGroup(String username, String groupName) {
        findGroupByName(groupName).ifPresent(group -> group.getUsers().stream()
                                                           .filter(member -> member.getUsername().equals(username))
                                                           .findFirst()
                                                           .ifPresent(user -> group.getUsers().remove(user)));
    }
}
