package org.nrg.xnat.analytics.pharos.metrics.resolvers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.nrg.xnat.analytics.pharos.core.resolvers.AbstractHandlerMethodArgumentResolver;
import org.nrg.xnat.analytics.pharos.core.utilities.RequestUtils;
import org.nrg.xnat.analytics.pharos.metrics.annotations.MetricsReportParam;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

@Component
@Slf4j
public class MetricsReportRequestResolver extends AbstractHandlerMethodArgumentResolver<MetricsReportParam, MetricsReport> {
    private final ObjectMapper _mapper;

    @Autowired
    public MetricsReportRequestResolver(final ObjectMapper objectMapper) {
        super(MetricsReportParam.class);
        _mapper = objectMapper;
        log.info("Created Metrics report request resolver");
    }

    @Override
    protected MetricsReport handleArgument(final HttpServletRequest request, final Map<String, String[]> parameters,
                                           final Map<String, List<MultipartFile>> files) {
        try {
            final String        content = IOUtils.toString(request.getInputStream(), Charset.forName(request.getCharacterEncoding()));
            final MetricsReport report  = _mapper.readValue(content, MetricsReport.class);
            report.setIpAddress(RequestUtils.getIpAddressFromRequest(request));
            log.debug("Received a new metrics report from UUID {} at IP {}", report.getUuid(), report.getIpAddress());
            return report;
        } catch (IOException e) {
            // FIXME: 2/15/22 Create app exceptions
            throw new RuntimeException(e);
        }
    }
}
