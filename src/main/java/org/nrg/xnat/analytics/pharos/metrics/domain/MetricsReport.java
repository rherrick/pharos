package org.nrg.xnat.analytics.pharos.metrics.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.nrg.xnat.analytics.pharos.core.domain.ImmutableEntity;
import org.nrg.xnat.analytics.pharos.metrics.resolvers.MetricsReportSerializer;
import org.springframework.data.annotation.Immutable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "metrics_reports")
@Immutable
@TypeDef(name = "json", typeClass = JsonType.class)
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@JsonPropertyOrder({"uuid", "xnatVersion", "ipAddress", "providerReports"})
@JsonSerialize(using = MetricsReportSerializer.class)
@Slf4j
public class MetricsReport extends ImmutableEntity {
    @NonNull
    @NotNull
    @Column(nullable = false)
    @Type(type = "uuid-char")
    private UUID uuid;

    @NonNull
    @NotNull
    @Column(nullable = false)
    private String xnatVersion;

    @NonNull
    @NotNull
    @Column(nullable = false)
    private String ipAddress;

    @JsonAlias("reports")
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private ArrayNode providerReports;
}
