package org.nrg.xnat.analytics.pharos.core.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.List;
import javax.persistence.*;

@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "groups")
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(name = "group_name", unique = true)
    private String groupName;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "group_authorities", joinColumns = @JoinColumn(name = "group_id"))
    @Column(name = "authority")
    private List<String> authorities;

    @ManyToMany
    @JoinTable(name = "group_members",
               joinColumns = {@JoinColumn(name = "group_id", referencedColumnName = "id")},
               inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    @Singular
    private List<User> users;
}
