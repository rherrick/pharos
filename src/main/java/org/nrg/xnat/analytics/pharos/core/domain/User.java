package org.nrg.xnat.analytics.pharos.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.*;

@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "users")
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(unique = true)
    private String username;

    @NonNull
    @JsonIgnore
    private String password;

    @Builder.Default
    private boolean enabled = true;

    @Transient
    @Builder.Default
    private boolean accountNonExpired = true;

    @Transient
    @Builder.Default
    private boolean accountNonLocked = true;

    @Transient
    @Builder.Default
    private boolean credentialsNonExpired = true;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "group_members",
               joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
               inverseJoinColumns = {@JoinColumn(name = "group_id", referencedColumnName = "id")})
    @Singular
    private List<Group> groups;

    @Transient
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return groups == null
               ? Collections.emptyList()
               : groups.stream().map(Group::getAuthorities).flatMap(Collection::stream).map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
    }
}
