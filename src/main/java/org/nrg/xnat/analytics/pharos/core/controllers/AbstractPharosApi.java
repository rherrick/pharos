package org.nrg.xnat.analytics.pharos.core.controllers;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

@Getter(AccessLevel.PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public abstract class AbstractPharosApi {
    protected Optional<UserDetails> getUser() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) {
            log.debug("Didn't find principal for current user");
            return Optional.empty();
        }
        if (principal instanceof UserDetails) {
            log.debug("Found principal for user: {}", ((UserDetails) principal).getUsername());
            return Optional.of((UserDetails) principal);
        }
        if (principal instanceof String && StringUtils.equals((String) principal, "anonymousUser")) {
            log.debug("Found principal for user, connecting as anonymousUser");
        } else {
            log.warn("Found principal for user but it's not a User: {} is a {}", principal, principal.getClass().getName());
        }
        return Optional.empty();
    }

    // TODO: Add sorting options here as well.
    protected Pageable page(final int page, final int size) {
        return page == 0 && size == 0 ? Pageable.unpaged() : PageRequest.of(page, size);
    }
}
