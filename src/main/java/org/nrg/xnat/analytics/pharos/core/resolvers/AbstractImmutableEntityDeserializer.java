package org.nrg.xnat.analytics.pharos.core.resolvers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.PharosApplication;
import org.nrg.xnat.analytics.pharos.core.domain.ImmutableEntity;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
public abstract class AbstractImmutableEntityDeserializer<E extends ImmutableEntity> extends StdDeserializer<E> {
    private static final List<String> ENTITY_FIELDS = Arrays.asList("id", "created", "createdBy");

    private final DateFormat _dateFormatter;

    protected AbstractImmutableEntityDeserializer(final Class<E> entityClass) {
        super(entityClass);
        _dateFormatter = new SimpleDateFormat(PharosApplication.COMMON_DATE_FORMAT);
    }

    protected boolean canHandleField(final String field) {
        return ENTITY_FIELDS.contains(field);
    }

    protected boolean handle(final JsonParser parser, final DeserializationContext context, final E instance, final String field) throws IOException {
        parser.nextToken();
        switch (field) {
            case "id":
                instance.setId(parser.getLongValue());
                break;
            case "created":
                final String created = parser.getText();
                try {
                    instance.setCreated(_dateFormatter.parse(created));
                } catch (ParseException e) {
                    throw InvalidFormatException.from(parser, "Invalid date format", created, Date.class);
                }
                break;
            case "createdBy":
                instance.setCreatedBy(parser.getText());
                break;
            default:
                log.warn("Unknown property for immutable entities: {}", field);
                return false;
        }
        return true;
    }
}