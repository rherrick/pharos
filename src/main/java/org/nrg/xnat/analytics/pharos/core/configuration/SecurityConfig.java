package org.nrg.xnat.analytics.pharos.core.configuration;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.nrg.xnat.analytics.pharos.core.controllers.PharosAccessDeniedHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import java.util.List;
import java.util.Map;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@Getter(AccessLevel.PROTECTED)
@Accessors(prefix = "_")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String[]               OPEN_PATHS          = new String[] {"/app", "/app/register", "/images/**/*", "/styles/**/*",
                                                                                   "/js/**/*", "/webjars/**/*"};
    public static final String[]               SUBMITTER_PATHS     = new String[] {"/crashpad/**/*", "/metrics/**/*"};
    public static final String[]               USER_PATHS          = new String[] {"/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html"};
    public static final String[]               ADMIN_PATHS         = new String[] {"/app/submitters", "/actuators/**"};
    public static final String                 ADMIN_GROUP         = "Admins";
    public static final String                 USER_GROUP          = "Users";
    public static final String                 SUBMITTER_GROUP     = "Submitters";
    public static final SimpleGrantedAuthority ADMIN_AUTHORITY     = new SimpleGrantedAuthority("ROLE_ADMIN");
    public static final SimpleGrantedAuthority USER_AUTHORITY      = new SimpleGrantedAuthority("ROLE_USER");
    public static final SimpleGrantedAuthority SUBMITTER_AUTHORITY = new SimpleGrantedAuthority("ROLE_SUBMITTER");

    public static final Map<String, List<GrantedAuthority>> ALL_AUTHORITIES = Map.of(ADMIN_GROUP, List.of(ADMIN_AUTHORITY, USER_AUTHORITY, SUBMITTER_AUTHORITY),
                                                                                     USER_GROUP, List.of(USER_AUTHORITY, SUBMITTER_AUTHORITY),
                                                                                     SUBMITTER_GROUP, List.of(SUBMITTER_AUTHORITY));
    public static final Map<String, String>                 DEFAULT_USERS   = Map.of("reader", "password",
                                                                                     "analyst", "password",
                                                                                     "admin", "xnat-pharos");

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers(OPEN_PATHS).permitAll()
            .antMatchers(SUBMITTER_PATHS).hasAnyAuthority(ADMIN_AUTHORITY.getAuthority(), USER_AUTHORITY.getAuthority(), SUBMITTER_AUTHORITY.getAuthority())
            .antMatchers(USER_PATHS).hasAnyRole(ADMIN_AUTHORITY.getAuthority(), USER_AUTHORITY.getAuthority())
            // .antMatchers(ADMIN_PATHS).hasRole(ADMIN_AUTHORITY.getAuthority())
            .and()
            .formLogin().loginPage("/login").permitAll()
            .and()
            .logout().permitAll()
            .and()
            .headers().frameOptions().disable()
            .and()
            .httpBasic()
            .and()
            .csrf().disable()
            .exceptionHandling()
            .accessDeniedHandler(accessDeniedHandler());
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new PharosAccessDeniedHandler();
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(final List<AuthenticationProvider> providers) {
        return new ProviderManager(providers);
    }
}
