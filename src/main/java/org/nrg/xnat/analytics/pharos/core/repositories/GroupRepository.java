package org.nrg.xnat.analytics.pharos.core.repositories;

import org.nrg.xnat.analytics.pharos.core.domain.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "groups", path = "groups")
public interface GroupRepository extends JpaRepository<Group, Long> {
    Group findByGroupName(final String groupName);

    boolean existsByGroupName(final String groupName);

    void deleteByGroupName(final String groupName);
}
