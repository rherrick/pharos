package org.nrg.xnat.analytics.pharos.crashpad.domain.projections;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;
import java.util.Map;

/**
 * Provides a version of the {@link CrashReport crash report} object that
 * doesn't include the dump file binary object.
 */
@Projection(name = "SimplifiedCrashReport", types = CrashReport.class)
@JsonPropertyOrder({"id", "productName", "productVersion", "platform", "processType", "electronVersion", "guid", "created", "createdBy", "hasDumpFile",
                    "extra"})
public interface SimplifiedCrashReport {
    @Value("#{target.id}")
    long getId();

    String getGuid();

    String getProductName();

    String getProductVersion();

    String getElectronVersion();

    String getPlatform();

    String getProcessType();

    String getCreatedBy();

    Date getCreated();

    Map<String, String> getExtra();

    @Value("#{target.getDumpFile() != null && target.dumpFile.length > 0}")
    boolean isHasDumpFile();
}
