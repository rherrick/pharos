package org.nrg.xnat.analytics.pharos.metrics.services;

import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.persistence.NoResultException;

@SuppressWarnings("unused")
public interface MetricsReportService {
    MetricsReport create(final MetricsReport report);

    MetricsReport get(final long id) throws NoResultException;

    List<MetricsReport> getAll();

    Page<MetricsReport> getAll(final Pageable page);

    List<MetricsReport> findByUuid(final String guid);

    Page<MetricsReport> findByUuid(final String guid, final Pageable page);

    List<MetricsReport> findByUuid(final UUID guid);

    Page<MetricsReport> findByUuid(final UUID guid, final Pageable page);

    List<MetricsReport> findByIpAddress(final String ipAddress);

    Page<MetricsReport> findByIpAddress(final String ipAddress, final Pageable page);

    List<MetricsReport> findByXnatVersion(final String xnatVersion);

    Page<MetricsReport> findByXnatVersion(final String xnatVersion, final Pageable page);

    /**
     * Deletes the report with the specified ID.
     *
     * @param id The ID of the report to be deleted.
     */
    void delete(final long id);

    /**
     * Deletes the specified report.
     *
     * @param report The report to be deleted.
     */
    void delete(final MetricsReport report);

    /**
     * Deletes all reports on the system.
     *
     * @return Returns the number of reports deleted from the system.
     */
    // FIXME: 3/8/22 These delete methods are only for development purposes. Remove them before release.
    long deleteAll();

    String serialize(final MetricsReport report);

    Optional<MetricsReport> deserialize(final String report);
}
