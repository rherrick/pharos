package org.nrg.xnat.analytics.pharos.metrics.repositories;

import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "metrics", path = "metrics")
public interface MetricsReportRepository extends JpaRepository<MetricsReport, Long> {
    MetricsReport readById(final long id);

    MetricsReport findDistinctTopByUuidOrderByCreatedDesc(final UUID uuid);

    List<MetricsReport> findAllByUuid(final UUID uuid);

    Page<MetricsReport> findAllByUuid(final UUID uuid, final Pageable page);

    List<MetricsReport> findAllByIpAddress(final String ipAddress);

    Page<MetricsReport> findAllByIpAddress(final String ipAddress, final Pageable page);

    List<MetricsReport> findAllByXnatVersion(final String xnatVersion);

    Page<MetricsReport> findAllByXnatVersion(final String xnatVersion, final Pageable page);

    List<MetricsReport> findAllByIpAddressAndXnatVersion(final String ipAddress, final String xnatVersion);

    Page<MetricsReport> findAllByIpAddressAndXnatVersion(final String ipAddress, final String xnatVersion, final Pageable page);

    List<MetricsReport> findAllByCreatedAfter(final Date created);

    Page<MetricsReport> findAllByCreatedAfter(final Date created, final Pageable page);

    List<MetricsReport> findAllByCreatedBefore(final Date created);

    Page<MetricsReport> findAllByCreatedBefore(final Date created, final Pageable page);

    List<MetricsReport> findAllByCreatedBetween(final Date start, final Date finish);

    Page<MetricsReport> findAllByCreatedBetween(final Date start, final Date finish, final Pageable page);
}
