package org.nrg.xnat.analytics.pharos.crashpad.services;

import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.domain.projections.SimplifiedCrashReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CrashReportService {
    CrashReport getFullCrashReport(final long id);

    List<CrashReport> findReportsByGuid(final String guid);

    Page<CrashReport> findReportsByGuid(final String guid, final Pageable page);

    List<CrashReport> findReportsByProductName(final String productName);

    Page<CrashReport> findReportsByProductName(final String productName, final Pageable page);

    List<CrashReport> findReportsByProductNameAndProductVersion(final String productName, final String productVersion);

    Page<CrashReport> findReportsByProductNameAndProductVersion(final String productName, final String productVersion, final Pageable page);

    SimplifiedCrashReport get(final long id);

    List<SimplifiedCrashReport> getAllSimplifiedReports();

    Page<SimplifiedCrashReport> getAllSimplifiedReports(final Pageable page);

    List<SimplifiedCrashReport> findSimplifiedReportsByGuid(final String guid);

    Page<SimplifiedCrashReport> findSimplifiedReportsByGuid(final String guid, final Pageable page);

    List<SimplifiedCrashReport> findSimplifiedReportsByProductName(final String productName);

    Page<SimplifiedCrashReport> findSimplifiedReportsByProductName(final String productName, final Pageable page);

    List<SimplifiedCrashReport> findSimplifiedReportsByProductNameAndProductVersion(final String productName, final String productVersion);

    Page<SimplifiedCrashReport> findSimplifiedReportsByProductNameAndProductVersion(final String productName, final String productVersion, final Pageable page);

    void create(final CrashReport report);

    Optional<CrashReport> findOneByGuid(String guid);
}
