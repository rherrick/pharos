package org.nrg.xnat.analytics.pharos.core.services;

import org.springframework.security.authentication.AuthenticationProvider;

public interface PharosAuthenticationProvider extends AuthenticationProvider {
}
