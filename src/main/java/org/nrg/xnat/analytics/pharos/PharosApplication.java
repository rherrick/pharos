package org.nrg.xnat.analytics.pharos;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.crashpad.resolvers.CrashReportRequestResolver;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.nrg.xnat.analytics.pharos.metrics.resolvers.MetricsReportDeserializer;
import org.nrg.xnat.analytics.pharos.metrics.resolvers.MetricsReportRequestResolver;
import org.nrg.xnat.analytics.pharos.metrics.resolvers.MetricsReportSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@SpringBootApplication
@EnableJpaAuditing
@EnableJpaRepositories
@Slf4j
public class PharosApplication implements WebMvcConfigurer {
    // TODO: Used for deserialize/serialize operations. Not a fan, fix this.
    public static final String COMMON_DATE_FORMAT = "dd-MM-yyyy hh:mm:ss";

    @Value("${info.app.version:unknown}")
    private String _pharosVersion;

    private ObjectMapper _mapper;

    public static void main(final String[] arguments) {
        SpringApplication.run(PharosApplication.class, arguments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "/swagger-ui/index.html");
        registry.addViewController("/login").setViewName("login");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addArgumentResolvers(final @NonNull List<HandlerMethodArgumentResolver> resolvers) {
        // TODO: Eventually this should done dynamically instead of being hard-wired, to allow for extending with other resolvers
        resolvers.add(new CrashReportRequestResolver());
        resolvers.add(new MetricsReportRequestResolver(_mapper));
    }

    @Bean
    public OpenAPI pharosOpenApi() {
        return new OpenAPI().components(new Components())
                            .info(new Info().title("XNAT Pharos")
                                            .description("This is the collection of API endpoints for working with XNAT Pharos")
                                            .version(_pharosVersion));
    }

    @Bean
    public SimpleModule simpleModule() {
        final SimpleModule module = new SimpleModule();
        module.addDeserializer(MetricsReport.class, new MetricsReportDeserializer());
        module.addSerializer(MetricsReport.class, new MetricsReportSerializer());
        return module;
    }

    @Bean
    public ObjectMapper objectMapper(final List<? extends com.fasterxml.jackson.databind.Module> modules) {
        _mapper = new ObjectMapper();
        modules.forEach(_mapper::registerModule);
        return _mapper;
    }
}
