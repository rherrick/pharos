# XNAT Pharos #

XNAT [Pharos](https://www.ancient.eu/Lighthouse_of_Alexandria) is a gateway server for the XNAT platform. The goal is to provide an extensible platform to
gather application metrics from both server and client applications, provide update and alert information to users and administrators, and generally comply with
the purposes of the Lighthouse of Alexandria as described by Pliny the Elder:

> The object of it is, by the light of its fires at night, to give warning to ships, of the
> neighbouring shoals, and to point out to them the entrance of the harbour.

Metrics collection provides a way to gather information from users of NRG and XNAT applications and give some visibility into our user base and installed deployments. This includes things like:

* How many active XNATs are out there?
* Where are they?
* What versions of XNAT, Java, and so forth are they using?
* What plugins are deployed?
* How much data is being managed?
* What client tools are deployed and on what platforms?
* What happens to these services and applications when they crash or otherwise act in unexpected ways?

## Architecture ##

Pharos is a Spring Boot application that prioritizes performance in capturing data over UI or visibility users outside of NRG. It has little or no contact with other services other than a PostgreSQL database. Although it should technically be compatible with any version of PostgreSQL that supports JSON and JSONB columns (i.e. 9.4 or later), it has been developed and tested with the latest available version. As of this writing that's 14.2.

> **Note:** Pharos requires Java 11 to run.

Pharos collects metrics data from outside users sending data to the server. Depending on which type of data is being sent, it may or may not require clients to register. Registration is required when submitting XNAT metrics data, although this process should be mostly or completely automated: the XNAT deployment that has the metrics plugin installed will detect that the installation isn't registered and submits the information to Pharos directly. That returns a GUID and access token to the client, which is used for subsequent submissions of metrics data.

Other types of metrics data may not support requiring authentication, e.g. submitting crashpad reports from Electron applications (e.g. XNAT Desktop Client) doesn't currently provide a way to authenticate. We'll need to find a way to prevent the service from being overwhelmed in the event someone tries a brute-force DoS attack by pushing large numbers of crashpad reports. For this reason, only authenticated metrics submissions are currently supported.

Clients in the field submit metrics data as JSON through standard HTTP exchanges.

> Pharos runs on port 9000 by default. This can be changed in the application itself or configured on the command line at launch time.

Other than a different default port, Pharos is exactly the same from the outside as any application running on Tomcat. Any standard proxy server can be used for the front-end to manage SSL termination, firewall pass-through, etc.

The diagram below shows the basic architecture of the Pharos service.

![Pharos Architecture](/assets/architecture.png)

## Deployment ##

The Pharos build is very flexible and can produce a war file, jar, application installation including service scripts, an OCI-compliant container image, and a docker-compose configuration. The NRG Jenkins CI service should build each of these artifacts, so that the latest war, jar, etc. will be available directly from the build server. The OCI image (which is compatible with Docker, Kubernetes, and Slurm) will be built on Jenkins and published to the NRG Docker Registry.

The primary requirement for deploying Pharos in any of these contexts is configuring database access. There are default values already configured in the application for database access, but those will only work when the database is also configured with the same default values:

| Setting  | Value                              |
| -------- | ---------------------------------- |
| URL      | jdbc:postgresql://localhost/pharos |
| Username | pharos                             |
| Password | pharos                             |

The format of the Pharos configuration is a simple properties file, with only the properties whose values differ from the defaults above required. The Pharos source repository includes a sample properties file that can be used as a basis for configuring specific deployments. Some deployment configurations may require you to specify the configuration file to use with the -Dspring.config.import command-line option. The value for this option can be any valid URL reachable from wherever you're launching the service, including HTTP/HTTPS URLs, file URLs or paths, etc.

The sections below describe how to configure the application in a few different deployment scenarios.

### Repository based ###

You can run directly from the code repository using the `gradlew` or `gradlew.bat` script:

```
./gradlew bootRun -Dspring.config.import=pharos.properties
```

This can be useful for testing things like database access without fully installing and configuring the application.

### Stand-alone application ###

To deploy Pharos as a stand-alone application, run java with the following options:

```
java -Dspring.config.import=pharos.properties -jar pharos-0.9-SNAPSHOT-boot.jar
```

Like the repository-based deployment, this is useful for testing deployment configurations and the like.

### Pharos distribution ###

A distributable archive can be built with one of the following commands:

```
./gradlew bootDistTar
./gradlew bootDistZip
```

These commands build a tar or zip file respectively in the folder build/distributions. The contents of each archive format are exactly the same. The unarchived distribution includes a bin folder containing the `pharos` and `pharos.bat` scripts. You can run this script directly, e.g. cd to the installation folder and run:

```
bin/pharos
```

You can also add the `bin` folder of your extracted Pharos distribution to your path and run just by typing the script name on the command line:

```
pharos
```

### Docker container ###

You can build an [OCI-compliant container image](https://opencontainers.org) for the Pharos application directly from the source repository:

```
./gradlew bootBuildImage
```

The `bootBuildImage` task requires that Docker be running on the same server as the build and that the user running the task have access to Docker (e.g. be a member of the docker group).

**Note:** Currently you can build the image from the Gradle configuration but can't yet publish the resulting image to the NRG Docker image repository. You can push newly built images to the registry with the following commands:

```
docker push registry.nrg.wustl.edu/docker/nrg-repo/pharos:<version>
docker push registry.nrg.wustl.edu/docker/nrg-repo/pharos:latest
```

Deploying as a stand-alone Docker container can be useful for testing, but requires setting the database properties to access a database outside of the Docker internal network. If the database is running on the same host as the Docker server, you can use the special host entry host.docker.internal in the database JDBC URL. Here's a sample command for launching the Pharos container:

```
docker run --detach --rm --publish 9000:9000 --mount type=bind,source="$(pwd)"/pharos.properties,target=/workspace/pharos.properties --name pharos registry.nrg.wustl.edu/docker/nrg-repo/pharos:latest
```

The configuration would look something like this:

```
spring.datasource.url=jdbc:postgresql://host.docker.internal/pharos
spring.datasource.username=pharos
spring.datasource.password=pharos
```

### Docker-compose ###

The Pharos repository also includes a docker-compose configuration that provisions the database and mounts the configuration in the Pharos container. Running the docker-compose configuration requires two extra files:

* An environment file per the docker-compose documentation, so either named `.env` or named explicitly with the `--env-file` option. The file `pharos.env.sample` can be used as a template.
* A configuration file as described in the deployment section above. The file `pharos.properties.sample` can be used as a template.

The git repository is configured to ignore files with the properties and env extensions, so you can start by copying those to new files without causing issues when trying to pull from the remote repository.

```
cp pharos.env.sample .env
cp pharos.properties.sample pharos.properties
```

The way these sample files are configured, simply copying them over as above should enable you to bring up the docker-compose configuration with a simple up command:

```
docker-compose up --detach
```

The values you'll need to change in a production deployment are:

* `ENV_PG_PASSWORD` specifies the master–or root or postgres–password for the database
* `ENV_PHAROS_DB_PASS` specifies the password for Pharos's database
* `spring.datasource.password` sets the value used by the Pharos service to access its database

You can change other values as needed as well, but these password settings are the most critical.

> **Note:** `ENV_PHAROS_DB_PASS` and `spring.datasource.password` will have the same value!

The environment file and Pharos configuration properties are only used on container launch and application start-up. For greater security, they can be deleted once the containers are up and running. They will need to be restored when restarting the services, however.

You can also make a lightweight docker-compose configuration (i.e. without the code, build tools, and other non-Docker-related files) with just the following files:

* `docker-compose.yml`
* `db/init.sh`
* `pharos.properties`
* Your environment file (e.g. `.env`)

You can install Pharos as a service on systemd-managed platforms using the [Pharos unit-definition file](pharos.service). To install this as a service:

1. Modify the unit-definition file with the appropriate user, group, and working directory. By default, the user is **pharos**, group is **docker**, and working directory is `/opt/pharos` (you can change the user or working directory, but the group probably must stay as **docker**).
1. Copy, move, or link the unit-definition file to `/etc/systemd/system/pharos.service`.
1. Run the command: `systemctl daemon-reload`
1. Run the command: `systemctl enable pharos.service`
1. Run the command: `systemctl start pharos.service`

You can verify proper functioning of the service as shown below:

```
# curl http://localhost:9000/app
Hello, I'm Pharos 1.0-SNAPSHOT running in mode(s) "prod"
 * OS:   Linux 5.4.0-104-generic amd64
 * Java: OpenJDK 64-Bit Server VM 11.0.14.1 BellSoft null

You are anonymous.
```

